<?php

function getFullAddress($countryAddress, $cityAddress, $provinceAddress, $specificAddress){
	return "$specificAddress, $cityAddress, $provinceAddress, $countryAddress";
}

function getLetterGrade($letterGrade){
	if($letterGrade < 75 ){
		return 'D';
	}
	else if($letterGrade >= 98 && $letterGrade <= 100){
		return 'A+';
	}
	else if($letterGrade >= 95 && $letterGrade <= 97){
		return 'A';
	}
	else if($letterGrade >= 92 && $letterGrade <= 94){
		return 'A-';
	}
	else if($letterGrade >= 89 && $letterGrade <= 91){
		return 'B+';
	}
	else if($letterGrade >= 86 && $letterGrade <= 88){
		return 'B';
	}
	else if($letterGrade >= 83 && $letterGrade <= 85){
		return 'B-';
	}
	else if($letterGrade >= 80 && $letterGrade <= 82){
		return 'C+';
	}
	else if($letterGrade >= 77 && $letterGrade <= 79){
		return 'C';
	}
	else if($letterGrade >= 75 && $letterGrade <= 76){
		return 'C-';
	}
}